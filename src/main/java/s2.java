import javax.swing.*;

public class s2 extends JFrame {


    JButton button;
    JTextField text1;
    JTextField text2;

    s2() {

        init();
    }

    public void init() {

        this.setLayout(null);
        setTitle("Multiply");
        setSize(300,150);
        setVisible(true);

        text1 = new JTextField();
        text1.setBounds(5,30,100,20);

        text2 = new JTextField();
        text2.setBounds(5,60,100,20);

        button = new JButton();
        button.setBounds(5,10,50,20);

        add(text1);add(text2);add(button);
    }

    public static void main(String[] args) {
        new s2();
    }
}